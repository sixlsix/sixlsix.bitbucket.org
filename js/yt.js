  		var cue = []; //"dVMO13CTsj4", "JCxMWWf4_Ww", "2WVVYUw2vbc", "UGIn7qNDLrU"];
  		var index = 0;
        var previousIndex = 0;
        var isPlaying = true;  		
        var tag = document.createElement('script');
        var currentSeekToTime;
        var currentVideoEntry;
        var isSeekedTo;
        isReady = false;

        previousDuration = null;

        var timeout = null;


        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
 
         var player;
 
        //execute as soon as the player API code downloads
        function onYouTubeIframeAPIReady() {
            player = new YT.Player('player', {
                height: '585',
                width: '960',
                videoId: cue[0],
                events: {
                    'onReady': onPlayerReady,
                    'onStateChange': onPlayerStateChange,
                    'onError': onPlayerError
                }
            });

            windowResize();
            
        }
 
        function getVideoDuration(videoID) {

            if ($("#checkboxChromecast").is(':checked')) {
                var videoId = videoID;
                var url = "http://gdata.youtube.com/feeds/api/videos/" + videoId + "?v=2&alt=jsonc";

                $.getJSON(url, function (data) {
                    var duration = (data.data.duration * 1000) + 5000;

                    if (duration != 0 && !isNaN(duration)) {
                        clearTimeout(timeout);
                        timeout = setTimeout(function () { playNextVideo() }, duration);
                        startTimeMS = (new Date()).getTime();

                        console.log("playing next video after: " + duration);
                    }
                
                }, 
            
                function () {
                
                });
            }
        }

        // execute when the onReady event fires
        //starts video
        function onPlayerReady(event) {
 
            duration = event.target.getDuration();

            isReady = true;
            
            //TODO: seperate title display from playing video so that i dont have to do the play/stop just to load the title for cued vid
            playAt(0);
            player.stopVideo();

            //player.cueVideoById(cue[0].id);

            $(window).scrollTop(0);

        }

        //  when the onStateChange event fires
        function onPlayerStateChange(event) {

            if ($("#checkboxChromecast").not(':checked'))
                clearTimeout(timeout);

            if (event.data == YT.PlayerState.PLAYING) {
                //console.log(YT);

                if(currentSeekToTime !== "0" && !isSeekedTo){
                    isSeekedTo = true;
                    player.seekTo(currentSeekToTime, true);
                }

            }else if (event.data == YT.PlayerState.ENDED) {
				playNextVideo();
            }
            // }else if (event.data == -1) {
            //     playNextVideo();
            // }


            //test for skipping videos longer than a minute
            var skipLength = document.getElementById("skipLength").value
            skipLength = skipLength * 60;

            if(skipLength > 0){
                if(player.getDuration() > skipLength){
                    playNextVideo();
                }            
            }


        }

        var isVideoError;

        function onPlayerError(event){

            isVideoError = true;
            
            $(currentVideoEntry).css("color", "#000000");
            $(currentVideoEntry).css("text-decoration", "line-through");
            $(currentVideoEntry).css("background-color", "rgba(255, 0, 0, 0.5)");

            playNextVideo();
        }

	   function playAt(videoIndex){

	       if(videoIndex >= 0 && videoIndex < cue.length)
	       {
	           //test for chromecast
	           //setTimeout(function () {

                   //get video length to set timer until play next video
	                getVideoDuration(cue[videoIndex].id) + 5000;

	                //var videoDuration = getVideoDuration(cue[videoIndex].id) + 5000;
	               //console.log(videoDuration);
               
	               player.cueVideoById(cue[videoIndex].id, 0, "large");

	               currentSeekToTime = 0;
	               isSeekedTo = false;


	               if (!isVideoError) {
	                   // remove previous border
	                   $("#" + previousIndex).css("border-color", "#1d1d1d");
	                   $("#" + previousIndex).css("border-width", "0px");
	                   $("#" + previousIndex).css("background-color", "rgba(0, 0, 0, 0.5)");
	                   $("#" + previousIndex).css("color", "#FFFFFF");

	               }

	               // reset error check for visual indication
	               isVideoError = false;

	               // $(".container").fadeOut("400", function() {
	               //     $(".container").css("background-image", "url(" + videoArray[videoIndex].thumb + ")");
	               //     $(".container").fadeIn("400");$(".container");
	               // });

	               //use lighter background for list entries using the default thumb
	               switch (videoArray[videoIndex].thumb) {
	                   case (thumbDefault): $(".container").css("background-image", "url(" + thumbLighter + ")");
	                       break;
	                   default: $(".container").css("background-image", "url(" + videoArray[videoIndex].thumb + ")");
	                       break;
	               }

	               $(".container").fadeIn("400"); $(".container");

	               index = videoIndex;
	               //player.loadVideoById(cue[videoIndex].id, 0, "large");

	               //test for chromecast
	               //player.cueVideoById(cue[videoIndex].id, 0, "large");


	               if (cue[videoIndex].time !== "") {
	                   currentSeekToTime = cue[videoIndex].time;
	               }

	               currentVideoEntry = "#" + videoIndex;

	               $(currentVideoEntry).css("color", "#000000");
	               $(currentVideoEntry).css("border-color", "#2d2d2d");
	               $(currentVideoEntry).css("border-width", "1px 0px 1px 1px");
	               $(currentVideoEntry).css("background-color", "rgba(255, 255, 255, 0.8)");


	               if (videoArray[videoIndex].title.length > 100) {

	                   var titleText = videoArray[videoIndex].title;

	                   $("#videoTitle").css('line-height', '39px');

	                   if (videoArray[videoIndex].title.length > 125) {
	                       $("#videoTitle").css('font-size', '18pt');

	                       if (videoArray[videoIndex].title.length > 270) {
	                           var truncatedTitle = titleText.substr(0, 270);
	                           truncatedTitle += "...";
	                           $("#videoTitle").text(htmlDecode(truncatedTitle));
	                       } else {
	                           $("#videoTitle").text(htmlDecode(titleText));
	                       }
	                   } else {
	                       $("#videoTitle").text(htmlDecode(titleText));
	                   }
	               } else {
	                   $("#videoTitle").css('line-height', '53px');
	                   $("#videoTitle").css('font-size', '30pt');
	                   $("#videoTitle").text(htmlDecode(videoArray[videoIndex].title));
	               }

	               $("#videoVoteCount").text(videoArray[videoIndex].upvotes + " upvotes");
	               $("#videoThreadUrl").attr("href", videoArray[videoIndex].thread);
	               $("#videoTitleUrl").attr("href", videoArray[videoIndex].externalUrl);
	               $("#videoCommentCount").text(videoArray[videoIndex].commentCount + " comments");
	               $("#videoSubreddit").text("/r/" + videoArray[videoIndex].subreddit);
	               $("#videoSubredditUrl").attr("href", "http://www.reddit.com/r/" + videoArray[videoIndex].subreddit);
	               $("#date").text(videoArray[videoIndex].date);

	               //$(currentVideoEntry)[0].scrollIntoView();

	               previousIndex = index;
	           //}, 0);

	           setTimeout(function () { playVideo();}, 1000);

	           if (player.PlayerState != YT.PlayerState.PLAYING) {
	               stopVideo();
	               playVideo();
               }

                //to avoid page scrolling up
                return false;
            }
		 }
		 
		 function playNextVideo(){

            if(index === cue.length-1){
                var playNextIndex = index;
                
                grabMore();
                playAt(playNextIndex);
            }else{
    			playAt(++index);
            }
            
            window.scrollBy(0,70);
		 }
 
 		 function playPreviousVideo(){

				if(index > 0)
				{
					playAt(--index);
                    window.scrollBy(0,-70);
	            }else{ index = 0;}
		 }

        function pauseVideo(){
            player.pauseVideo();
            isPlaying = false;
        }

        function playVideo(){
            player.playVideo();
            isPlaying =  true;
        }

        function playPause(){
            if (isPlaying) {
                //if playing
                pauseVideo();        
            }else {
                //if paused
                playVideo();
            }
        }

        function stopVideo(){
            player.stopVideo();
            player.seekTo(0, false);        
            isPlaying = false;
        }

        function stopPlay(){
            if (isPlaying) {
                //if playing
                player.stopVideo();
                //player.seekTo(0, false);        

            }else{
                //if paused
                playVideo();
            }
        }

         function volumeDown(){
            player.setVolume(player.getVolume() - 5);
         }
		 
         function volumeUp(){
            player.setVolume(player.getVolume() + 5);
         }

         function muteUnmute(){
            if(player.isMuted()){
                player.unMute();
            }else{
                player.mute();
            }
         }

         function forwardVideo(){
            player.seekTo(player.getCurrentTime() + 5);
         }

         function forwardMoreVideo(){
            player.seekTo(player.getCurrentTime() + 25);
         }

         function rewindVideo(){
            player.seekTo(player.getCurrentTime() - 5);
         }

         function rewindMoreVideo(){
            player.seekTo(player.getCurrentTime() - 25);
         }

        var previous = $("#previous"),
            next = $("#next");
 
        previous.on('click', function () {
            playPreviousVideo();
        });
 
        next.on('click', function () {
        	playNextVideo();
        });       
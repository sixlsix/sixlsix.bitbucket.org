//shuffle array
function shuffle(array){
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
}

function shuffleVideos(){

	//clear list!!!
	document.getElementById("list").innerHTML = "";

	var shuffledArray = shuffle(videoArray);

	//empty array
	cue.length = 0;

	for(i = 0; i < shuffledArray.length; i++){
 
		var entryThumb = shuffledArray[i].thumb;
		var entryTitle = shuffledArray[i].title;
		var entryUrl = shuffledArray[i].url;
		var entryTime = shuffledArray[i].time;
		

		if(document.getElementById("list")){
			document.getElementById("list").innerHTML += "<a href='javascript:void(0)' onClick='playAt(" + i + ")'>" +
				"<div class='listItem' id='" + i + "'>" + 
				"<img class='listItemThumb' src = '" + entryThumb + "'/>" +
				"<div class='listItemTitle'>" + entryTitle + "</div></div></a>";

			//cue.push(entryUrl);

			if(entryUrl !== null){
				cue.push({id: entryUrl[0], time: entryTime});
			}else{
				cue.push({id: "", time: ""});
			}
		}
	}
}

function checkEnter(e){
		 e = e || event;
		 var enterPressed = (e.keyCode || event.which || event.charCode || 0) !== 13;
		 if(!enterPressed){
			subFetch();
		 }              	
		 
		 return enterPressed;
	}


/**
 * JavaScript function to match (and return) the video Id 
 * of any valid Youtube Url, given as input string.
 * @author: Stephan Schmitz <eyecatchup@gmail.com>
 * @url: http://stackoverflow.com/a/10315969/624466
 */
function ytVidId(url) {
  var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
  return (url.match(p)) ? RegExp.$1 : false;
}

//decodes encoded values like '&amp;' back to &
function htmlDecode(html) {
    var div = document.createElement("div");
    div.innerHTML = html;
    return div.childNodes[0].nodeValue;
}

var reddit = "https://www.reddit.com/r/";
var subValue = "";
var url= "";
var counter = 0;
sortMethod = "";

//global definition
thumbNSFW = "https://i.imgur.com/hMXdoJC.jpg";
thumbDefault = "https://i.imgur.com/7JlcPc7.jpg";
thumbLighter = "https://i.imgur.com/N9Kads.jpg";

subFetch = function(subLink){
	counter = 0;
	grabMoreCounter = 0;

	subEntry = document.getElementById("subreddit").value;

	if(sortMethod === undefined)
		sortMethod = "";

	if(subLink !== undefined && subLink !== ""){
		subValue = subLink;
		url = reddit + subValue + "/" + sortMethod + ".json?limit=100&jsonp=grabIt";
	}else if(subEntry === ""){
		subValue = "";
		url = "https://www.reddit.com/" + sortMethod + ".json?limit=100&jsonp=grabIt";		
	}else if(subEntry == "saved"){
	 	// url = "http://www.reddit.com/user/nopenever/saved/.json?limit=100";

	 	// $.getJSON( url, function( data ) {
			//   var items = [];
			//   $.each( data, function( key, val ) {
			//     items.push( "<li id='" + key + "'>" + val + "</li>" );
			//   });
			 
			//   $( "<ul/>", {
			//     "class": "my-new-list",
			//     html: items.join( "" )
			//   }).appendTo( "body" );
			// });		
	}else{
			subValue = document.getElementById("subreddit").value;
			url = reddit + subValue + "/.json?limit=100&jsonp=grabIt";	
	}

	//clear list!!!
	document.getElementById("list").innerHTML = "";			
	
	var head= document.getElementsByTagName('head')[0];
	var script= document.createElement('script');
	script.type= 'text/javascript';
	script.src= url;
	head.appendChild(script);

	$('#subreddit').val(subValue);

	$(window).scrollTop(0);

	//$('#0').focus();


	//autoplay first vid
	// if(videoArray.length >= 1){
	//  	playAt(0);
	// }
};

var videoArray;
var isLoadingMore;
var grabMoreCounter = 0;

grabMore = function(){
	
	++grabMoreCounter;
	isLoadingMore = true;
	var test = $('#subreddit').text();

	 if(subValue === ""){
	 	//load more for front page (subreddit text box is empty)
	 	url = "https://www.reddit.com/" + sortMethod + ".json?limit=100&after=" + entryName + "&jsonp=grabIt";
	 }else{
		url = reddit + subValue + "/" + sortMethod + "/.json?limit=100&after=" + entryName + "&jsonp=grabIt";
	 }
	
	var head= document.getElementsByTagName('head')[0];
	var script= document.createElement('script');
	script.type= 'text/javascript';
	script.src= url;
	head.appendChild(script);

	//$.waypoints('refresh');

};

grabIt = function(dataX) {
	
	var initialPic = 0;
	var isYTLink = false;
	
	if(!isLoadingMore){
		cue = [];
		videoArray = new Array();
	}

	for(i = 0; i < dataX.data.children.length; i++){
		var entryTitle = dataX.data.children[i].data.title;
		var entryTitleStripped = ""; //strips out bad quote characters from entry title
		var entryUrl = dataX.data.children[i].data.url; //this gets stripped down to its youtube id
		var externalUrl = dataX.data.children[i].data.url; //unmodified url
		entryName = dataX.data.children[i].data.name; //used for after/before navigation of thread pages
		entryDate = new Date(dataX.data.children[i].data.created_utc * 1000);
		entryDateFormatted = entryDate.toDateString();

		var entryTime = "";
		var entryComments = "http://www.reddit.com" + dataX.data.children[i].data.permalink;
		var entrySubreddit = dataX.data.children[i].data.subreddit;
		var entryThumb = dataX.data.children[i].data.thumbnail;
		var entryUpvotes = dataX.data.children[i].data.score;
		var entryCommentCount = dataX.data.children[i].data.num_comments;
		var entrySelfText = dataX.data.children[i].data.selftext;
		var entryNoImageThumb = "";
		var spriteThumb = "";

		if(/.+youtube.com.+/.test(entryUrl)){
			isYTLink = true;
			
			try{
				//entryUrl = ytVidId(entryUrl);
				//entryUrl = "https://www.youtube.com/watch?v=RCQmgjLhIZw#t=39";

				if (/.+\#t\=.+/.test(entryUrl)){
					var splitUrl = entryUrl.split("#t=");
					var urlPattern = /.{11}$/;
					entryTime = splitUrl[1];
					entryUrl = splitUrl[0].match(urlPattern);

				}else if(/.+\/v\/.+/.test(entryUrl)){
					var splitUrl = entryUrl.split("/v/");
					var urlPattern = /.{11}/;
					
					if(splitUrl.length > 0){
				 		entryUrl = splitUrl[1].match(urlPattern);
					}
				 
				}else if(/.+v\%3D.+/.test(entryUrl)){
					var splitUrl = entryUrl.split("v%3D");
					var urlPattern = /.{11}/;
					
					if(splitUrl.length > 0){
				 		entryUrl = splitUrl[1].match(urlPattern);
					}
				}else if(/.+player_embedded.+/.test(entryUrl)){
					//if issues check for ampersand versus amp; in returned entryUrl
					var splitUrl = entryUrl.split("v=");
					var urlPattern = /.{11}/;
					
					if(splitUrl.length > 0){
				 		entryUrl = splitUrl[1].match(urlPattern);
					}
				}else if(/.+watch\?v.+/.test(entryUrl)){
					var splitUrl = entryUrl.split("watch?v=");
					var urlPattern = /.{11}/;
					
					if(splitUrl.length > 0){
				 		entryUrl = splitUrl[1].match(urlPattern);
					}
				}
			}
			catch(Exception){
				
			}
			 
		}else if(/.+youtu.be.+/.test(entryUrl)){
			isYTLink = true;
			
			try{
				if (/.+youtu\.be\/.+/.test(entryUrl)){
					var splitUrl = entryUrl.split("youtu.be/");
					var urlPattern = /.{11}/;
					if(splitUrl.length > 0){
						entryUrl = splitUrl[1].match(urlPattern);
			 			//console.log("shortUrl  --- " + entryUrl);
			 		}
				}
				
			}
			catch(Exception){
				
			}
		}else{
			isYTLink = false;
			continue;
		}
			

		//checks if nsfw/default/self and assigns correct thumb
		if(entryThumb === "nsfw"){
			entryThumb = thumbNSFW;
		}else if((entryThumb === "default") || (entryThumb === "self") || (entryThumb === "")){
			entryThumb = thumbDefault;
		}else {
			entryThumb = entryThumb;
		 }

		//strips out bad quote characters form entry title
		entryTitleStripped = entryTitle.replace(/[^\w. -]/g,'');


	 	currentEntry = {
			title: entryTitle,
			url: entryUrl,
			externalUrl: externalUrl,
			date: entryDateFormatted,
			time: entryTime,
			subreddit: entrySubreddit,
			thread: entryComments,
			thumb: entryThumb,
			upvotes: entryUpvotes,
			commentCount: entryCommentCount,
			selftText: entrySelfText
		}


		var x = "meh";
		
		// var picFunction = "'picViewer(" + '"' + entryUrl + '", "' + entryTitleStripped /*entryTitle*/ +  '", "' + entryComments + '"' + ")'";
		// var div = "<div onfocus=" + picFunction + " onclick=" + picFunction + " >";
		// var ahref = "<a href=''javascript:void(0)'' alt='blah' title='" + entryTitleStripped + "'>";
		// var img = "<img class='picThumb " + entryNoImageThumb + "' " + entryThumb + "/>";
	
		//document.getElementById("main").innerHTML += div + img + '</a></div>';
		
		 if(document.getElementById("list") && isYTLink){
			document.getElementById("list").innerHTML += "<a href='javascript:void(0)' onClick='playAt(" + counter + ")'>" +
				"<div class='listItem' id='" + counter + "'><img class='listItemThumb' " + 
			"src = '" + entryThumb + "'/><div class='listItemTitle'>" + entryTitle + "</div></div></a>";

			videoArray.push(currentEntry);

			if(i === i < dataX.data.children.length - 1){

			}
		}
		
		counter++;
		if(entryUrl !== null){
			cue.push({id: entryUrl[0], time: entryTime});
		}else{
			cue.push({id: "", time: ""});
		}
		entryThumb = "";
		entryTime= "";

	}

		if(!isLoadingMore){
			
			index = 0;

			if(videoArray[0] !== undefined){
				
				//checks for nsfw thumb and uses lighter image for background
				if(videoArray[0].thumb === thumbNSFW || videoArray[0].thumb === thumbDefault){
				    $(".container").css("background-image", "url(" + thumbLighter + ")");	
				}else{
				    $(".container").css("background-image", "url(" + videoArray[0].thumb + ")");
	        	}

	        	$(".container").fadeIn("100");$(".container");
			}
		}

		// for playing first video
		// if($("#autoplay").checked){
        // if(document.getElementById("autoplay").checked === true){
        //     playAt(0);
        // }else{
        //     playAt(0);
        //     player.stopVideo();
        // }

		$("#btn-group").focus();

		if(isReady  && !isLoadingMore){
        	//player.cueVideoById(cue[0].id);

	        if(document.getElementById("autoplay").checked === true){
	            playAt(0);
	        }else{

            	$(window).scrollTop(0);

	            //playAt(0);
	            //player.stopVideo();
	        }

		}

		//reset for loading normal vs more
		isLoadingMore = false;

		//
		if(cue.length < 50 && grabMoreCounter < 5){
			grabMore();
		}

		$('#videoCount').text(cue.length);
};